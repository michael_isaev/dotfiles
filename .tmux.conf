# 256 colors for vim
set -g default-terminal "screen-256color"

set -g history-limit 20000

# Prevent exiting on Ctrl-D just immediately
# IGNOREEOF=10   # Shell only exists after the 10th consecutive Ctrl-d
# set -o ignoreeof  # Same as setting IGNOREEOF=10
# bind-key -n C-d detach

# Mouse works as expected
# set -g mouse on
set -g mode-mouse on
set -g mouse-select-pane on
set -g mouse-resize-pane on
set -g mouse-select-window on

# unbind default prefix and set it to Ctrl+a
unbind C-b
set -g prefix C-a
bind C-a send-prefix

# for nested tmux sessions
bind-key a send-prefix

# Rather than constraining window size to the maximum size of any client
# connected to the *session*, constrain window size to the maximum size of any
# client connected to *that window*. Much more reasonable.
setw -g aggressive-resize on

# UTF is great, let us use that
set -g utf8
set-window-option -g utf8 on

# command delay? We don't want that, make it short
set -sg escape-time 0

# Start window numbering at 1
set-option -g base-index 1
set-window-option -g pane-base-index 1

# Ctrl-Tab to switch windows
bind-key -n C-Tab next-window
bind-key -n C-S-Tab previous-window

# Cycle panes with C-a C-a
unbind C-a
bind C-a select-pane -t :.+

# Reload config with a key
bind-key r source-file ~/.tmux.conf \; display "Config reloaded!"

# quickly open a new window
#bind N new-window

# use PREFIX | to split window horizontally and PREFIX - to split vertically
bind | split-window -h
bind - split-window -v

# Remap window navigation to vim
unbind-key j
bind-key j select-pane -D
unbind-key k
bind-key k select-pane -U
unbind-key h
bind-key h select-pane -L
unbind-key l
bind-key l select-pane -R

# resize panes using PREFIX H, J, K, L
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

# Smart pane switching with awareness of Vim splits.
# See: https://github.com/christoomey/vim-tmux-navigator
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
bind-key -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"

# unbind-key C-Up
# unbind-key C-Down
# unbind-key C-Left
# unbind-key C-Right

# bind-key -n C-Up if-shell "$is_vim" "send-keys C-Up" "resize-pane -U"
# bind-key -n C-Down if-shell "$is_vim" "send-keys C-Down" "resize-pane -D"
# bind-key -n C-Left if-shell "$is_vim" "send-keys C-Left" "resize-pane -L"
# bind-key -n C-Right if-shell "$is_vim" "send-keys C-Right" "resize-pane -R"

# Use vim keybindings in copy mode
setw -g mode-keys vi

# more settings to make copy-mode more vim-like
unbind [
bind Escape copy-mode
unbind p
bind p paste-buffer
bind -t vi-copy 'v' begin-selection
bind -t vi-copy 'y' copy-selection
bind -t vi-copy y copy-pipe "xclip -sel clip i"

# Setup 'v' to begin selection as in Vim
# bind-key -t vi-copy v begin-selection
# bind-key -t vi-copy y copy-pipe "xclip -sel clip i"
# bind-key -t vi-copy y copy-pipe "reattach-to-user-namespace pbcopy"

# Change previous window key
bind-key N previous-window
# ----------------------
# set some pretty colors
# ----------------------
# set pane colors - hilight the active pane
#set-option -g pane-border-fg colour235 #base02
#set-option -g pane-active-border-fg colour240 #base01

# colorize messages in the command line
#set-option -g message-bg black #base02
#set-option -g message-fg brightred #orange

# ----------------------
# Status Bar
# -----------------------
set-option -g status on                # turn the status bar on
set -g status-utf8 on                  # set utf-8 for the status bar
set -g status-interval 5               # set update frequencey (default 15 seconds)
set -g status-justify centre           # center window list for clarity
# set-option -g status-position top    # position the status bar at top of screen

# visual notification of activity in other windows
setw -g monitor-activity on
set -g visual-activity on

# set color for status bar
set-option -g status-bg colour235 #base02
set-option -g status-fg yellow #yellow
set-option -g status-attr dim

# set window list colors - red for active and cyan for inactive
set-window-option -g window-status-fg brightblue #base0
set-window-option -g window-status-bg colour236
set-window-option -g window-status-attr dim

set-window-option -g window-status-current-fg brightred #orange
set-window-option -g window-status-current-bg colour236
set-window-option -g window-status-current-attr bright

# show host name and IP address on left side of status bar
set -g status-left-length 70
set -g status-left "#[fg=green]: #h : #[fg=brightblue]#(curl icanhazip.com) #[fg=yellow]#(ifconfig en0 | grep 'inet ' | awk '{print \"en0 \" $2}') #(ifconfig en1 | grep 'inet ' | awk '{print \"en1 \" $2}') #[fg=red]#(ifconfig tun0 | grep 'inet ' | awk '{print \"vpn \" $2}') "

# show session name, window & pane number, date and time on right side of
# status bar
set -g status-right-length 60
set -g status-right "#[fg=brightblue]#S #I:#P #[fg=yellow]:: %d %b %Y #[fg=green]:: %l:%M %p ::"

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
#set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'tmux-plugins/tmux-yank'

set -g @continuum-restore 'on'
set -g @yank_selection 'primary'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'git@github.com/user/plugin'
# set -g @plugin 'git@bitbucket.com/user/plugin'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
