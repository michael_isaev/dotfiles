" ================ Color Scheme ===========================
set background=dark
syntax on
if has('gui_running')
  colorscheme darkspectrum
else
  se t_Co=256
  let g:solarized_termcolors=256
  "let base16colorspace=256  " Access colors present in 256 colorspace"
  colorscheme hybrid "base16-monokai
  "colorscheme solarized
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if (&t_Co > 2 || has("gui_running")) && !exists("syntax_on")
  syntax on
endif

" Hotkeys

" Open file
nnoremap <Leader>o :CtrlP<CR>
" Close file
nnoremap <Leader>w :w<CR>
" Quit file
nnoremap <Leader>q :q<CR>

" Folding rules
set foldmethod=indent
set foldcolumn=4
set foldlevel=99

" Enable folding with the spacebar
nnoremap <leader><space> za

" Display extra whitespace
set list listchars=tab:»·,trail:·,eol:¬

" spell check
set spell spelllang=en_us

" remove highlighting in spell check
hi clear SpellBad
hi SpellBad cterm=underline



" ================ Basic Setup ======================
set nocompatible              " be iMproved, required
filetype off                  " required
set number
set backspace=2   " Backspace deletes like most programs in insert mode
set nobackup
set nowritebackup
set noswapfile    " http://robots.thoughtbot.com/post/18739402579/global-gitignore#comment-458413287
set history=500
set ruler         " show the cursor position all the time
set showcmd       " display incomplete commands
set showmode      "Show current mode down the bottom
set gcr=a:blinkon0 "Disable cursor blink
set visualbell    "No sounds
set laststatus=2  " Always display the status line
set autoread                    "Reload files changed outside vim
set autowrite     " Automatically :write before running commands
set mouse=a
set ttyfast
set clipboard=unnamedplus
" This makes vim act like all other editors, buffers can
" exist in the background without being in a window.
" http://items.sjbach.com/319/configuring-vim-right
set hidden

" ================ Indentation ============================

set autoindent
set cindent
set smartindent
set smarttab
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab

" ================ Scrolling ==============================

set scrolloff=4         "Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=1

" ================ Search =================================

set incsearch       " Find the next match as we type the search
set hlsearch        " Highlight searches by default
set ignorecase      " Ignore case when searching...
set smartcase       " ...unless we type a capital

" let mapleader = "\<Space>"
map <SPACE> <leader>

set colorcolumn=81
highlight ColorColumn ctermbg=darkgray

" Fuzzy finder: ignore stuff that can't be opened, and generated files
let g:fuzzy_ignore = "*.png;*.PNG;*.JPG;*.jpg;*.GIF;*.gif;vendor/**;coverage/**;tmp/**;rdoc/**"

" ================ Plugins ================================
"
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Autosave sessions for vim
Plugin 'xolox/vim-session'
Plugin 'xolox/vim-misc'
" let g:session_autoload = 'yes'
" let g:session_autosave = 'yes'
let g:session_default_to_last = 1
let g:session_autosave_periodic = 1
" nmap <leader>r :RestartVim<CR>
nmap <leader>os :OpenSession<CR>
" nmap <leader>s :SaveSession<CR>

" ================ Appearance =============================
" nice airline-like ststus bar
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" Enable buffer display for airline
let g:airline#extensions#ctrlp#show_adjacent_modes = 1
let g:airline#extensions#spellcheck#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#branch#empty_message = ''
let g:airline#extensions#ycm#enabled = 1
let g:airline#extensions#tagbar#enabled = 1
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
    endif
    let g:airline_symbols.space = "\ua0"
    let g:airline_theme='wombat'
    set t_Co=256

" Base16 color schemes
Plugin 'chriskempson/base16-vim'

" Show indent guides, toggle with ,ig
Plugin 'nathanaelkane/vim-indent-guides'
let g:indent_guides_enable_on_vim_startup=0
let g:indent_guides_color_change_percent=20
let g:indent_guides_exclude_filetypes=['help', 'nerdtree']
let g:indent_guides_start_level=2
let g:indent_guides_guide_size=1
" let g:indent_guides_auto_colors=0
" hi IndentGuidesEven ctermbg=darkgray
" hi IndentGuidesOdd  ctermbg=black
set ts=2 sw=2 et

" ================ TMUX and console features ==============

" Better tmux integration
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'benmills/vimux'

" Open new split panes to right and bottom, which feels more natural
set splitbelow
set splitright

" Easy navigation between splits. Instead of ctrl-w + j. Just ctrl-j
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" resize current buffer by +/- 5 
nnoremap <silent> <C-Left> :vertical resize +5<cr>
nnoremap <silent> <C-Down> :resize -5<cr>
nnoremap <silent> <C-Up> :resize +5<cr>
nnoremap <silent> <C-Right> :vertical resize -5<cr>

" No, bad, very bad idea
" nnoremap <Tab> <c-w>w
" nnoremap <bs> <c-w>W

" Get off my lawn
" nnoremap <Left> :echoe "Use h"<CR>
" nnoremap <Right> :echoe "Use l"<CR>
" nnoremap <Up> :echoe "Use k"<CR>
" nnoremap <Down> :echoe "Use j"<CR>

" Persistent undo
set undodir=~/.vim/undo/
set undofile
set undolevels=1000
set undoreload=10000

" Git plugin to show changes in gutter
Plugin 'airblade/vim-gitgutter'

" plugin for GitHub integration
Plugin 'tpope/vim-fugitive'

" ================ IDE functionality ======================

Plugin 'tpope/vim-vinegar'
" NerdTREE
Plugin 'scrooloose/nerdtree'
" a git plugin for NerdTREE
Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'scrooloose/nerdcommenter'

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
let NERDTreeShowHidden = 1
map <F2> :NERDTreeToggle<CR>

" Fuzzy search plugin
Plugin 'ctrlpvim/ctrlp.vim'
let g:ctrlp_map = '<c-p>'
map <C-b> :CtrlPBuffer<CR>
" let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_working_path_mode = 'ra'
set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }

" Snippets for lots of languages
Plugin 'honza/vim-snippets'
Plugin 'SirVer/ultisnips'

" YouCompleteMe - Code completion
Plugin 'Valloric/YouCompleteMe'
Plugin 'rdnetto/YCM-Generator'
"nnoremap <leader>jd :YcmCompleter GoToDefinitionElseDeclaration<CR>
"nnoremap <F5> :YcmForceCompileAndDiagnostics<CR>
set shortmess+=c
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
"Do not ask when starting vim
let g:ycm_confirm_extra_conf = 0
let g:ycm_use_ultisnips_completer = 1
" let g:syntastic_always_populate_loc_list = 1
let g:ycm_collect_identifiers_from_tags_files = 1
set tags+=./.tags
let g:ycm_key_detailed_diagnostics = '<leader>dd'

" Fix YCM with ultisnippets issue
Plugin 'ervandew/supertab'
" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" ================ New Motions ============================

" ack utilite
Plugin 'mileszs/ack.vim'

" Another search plugin
" Plugin 'rking/ag.vim'

" Tim Pope plugin set
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-endwise'
Plugin 'tpope/vim-obsession'

" Brakets/quates closing
Plugin 'Raimondi/delimitMate'

" Add text object for indentations
Plugin 'michaeljsmith/vim-indent-object'

" Vim support for CamelCase
Plugin 'bkad/CamelCaseMotion'
" call CamelCaseMotion#CreateMotionMappings('<leader>')
" Good idea, but bad implementation of words with braces
" map <silent> w <Plug>CamelCaseMotion_w
" map <silent> b <Plug>CamelCaseMotion_b
" map <silent> e <Plug>CamelCaseMotion_e
" map <silent> ge <Plug>CamelCaseMotion_ge
" sunmap w
" sunmap b
" sunmap e
" sunmap ge
" omap <silent> iw <Plug>CamelCaseMotion_iw
" xmap <silent> iw <Plug>CamelCaseMotion_iw
" omap <silent> ib <Plug>CamelCaseMotion_ib
" xmap <silent> ib <Plug>CamelCaseMotion_ib
" omap <silent> ie <Plug>CamelCaseMotion_ie
" xmap <silent> ie <Plug>CamelCaseMotion_ie

" Motion plugin for f/t-like search
Plugin 'justinmk/vim-sneak'
" let g:sneak#streak = 1
" Replace f with Sneak - multi-line search for several characters
nmap f <Plug>Sneak_s
nmap F <Plug>Sneak_S
xmap f <Plug>Sneak_s
xmap F <Plug>Sneak_S
omap f <Plug>Sneak_s
omap F <Plug>Sneak_S
"replace 'f' with 1-char Sneak
nmap f <Plug>Sneak_f
nmap F <Plug>Sneak_F
xmap f <Plug>Sneak_f
xmap F <Plug>Sneak_F
omap f <Plug>Sneak_f
omap F <Plug>Sneak_F
"replace 't' with 1-char Sneak
nmap t <Plug>Sneak_t
nmap T <Plug>Sneak_T
xmap t <Plug>Sneak_t
xmap T <Plug>Sneak_T
omap t <Plug>Sneak_t
omap T <Plug>Sneak_T

" Multiple cursors like sublime block mode
Plugin 'terryma/vim-multiple-cursors'

" ================ C++ Section ============================

" C++ syntax highlight
Plugin 'octol/vim-cpp-enhanced-highlight'

" Switch between source and header file
Plugin 'vim-scripts/a.vim'

" Managing ctags
Plugin 'majutsushi/tagbar'
nmap <F8> :TagbarToggle<CR>
Plugin 'xolox/vim-easytags'

" Valgrind plugin
Plugin 'vim-scripts/valgrind.vim'
map <leader>v :Valgrind

" GDB frontend
" ConqueTerm and ConqueGDB installed from vbm folder
map <silent!> <F10> :ConqueGdb
let g:ConqueGdb_SrcSplit = 'above'
nnoremap <silent> <Leader>Y :ConqueGdbCommand y<CR>
nnoremap <silent> <Leader>N :ConqueGdbCommand n<CR>
let g:ConqueGdb_Leader = '<Leader>'
let g:ConqueGdb_DeleteBreak = g:ConqueGdb_Leader.'d'

" Clang code beautifier (code style format)
Plugin 'rhysd/vim-clang-format'
Plugin 'kana/vim-operator-user'

let g:clang_format#style_options = {
            \ "AccessModifierOffset" : -4,
            \ "AllowShortIfStatementsOnASingleLine" : "true",
            \ "AlwaysBreakTemplateDeclarations" : "true",
            \ "Standard" : "C++11"}

" map to <Leader>cf in C++ code
autocmd FileType c,cpp,objc nnoremap <buffer><Leader>cf :<C-u>ClangFormat<CR>
autocmd FileType c,cpp,objc vnoremap <buffer><Leader>cf :ClangFormat<CR>
" if you install vim-operator-user
autocmd FileType c,cpp,objc map <buffer><Leader>x <Plug>(operator-clang-format)
" Toggle auto formatting:
nmap <Leader>C :ClangFormatAutoToggle<CR>
autocmd FileType c ClangFormatAutoEnable
let g:clang_format#command="clang-format-3.6"

" ================ Python Section =========================

" Folding for Python
Plugin 'tmhedberg/SimpylFold'
let g:SimpylFold_docstring_preview = 1
let g:SimpylFold_fold_docstring = 0
let g:SimpylFold_fold_import = 0

" Python yapf formatter
Plugin 'mindriot101/vim-yapf'

" Python intendation
Plugin 'vim-scripts/indentpython.vim'

" ==========================================================

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

let g:airline_section_z = airline#section#create(['%{ObsessionStatus(''$'', '''')}', 'windowswap', '%3p%% ', 'linenr', ':%3v '])
